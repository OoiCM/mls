<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Agent related api + controller
Route::post('/agent/create', 'Agent\ExternalAPiController@createAgentProcess');
Route::post('/agent/view', 'Agent\ExternalAPiController@getAgentProcess');
Route::post('/agent/edit', 'Agent\ExternalAPiController@editAgentProcess');
Route::post('/agent/delete', 'Agent\ExternalAPiController@deleteAgentProcess');

// Agent related schedule api + controller
Route::post('/agent/create/schedule', 'Agent\ExternalAPiController@createScheduleProcess');
Route::post('/agent/view/schedule', 'Agent\ExternalAPiController@getScheduleProcess');
Route::post('/agent/edit/schedule', 'Agent\ExternalAPiController@editScheduleProcess');
Route::post('/agent/delete/schedule', 'Agent\ExternalAPiController@deleteScheduleProcess');

// Manager related api + controller
Route::post('/manager/create', 'Manager\ExternalAPiController@createManagerProcess');
Route::post('/manager/view', 'Manager\ExternalAPiController@getManagerProcess');
Route::post('/manager/edit', 'Manager\ExternalAPiController@editManagerProcess');
Route::post('/manager/delete', 'Manager\ExternalAPiController@deleteManagerProcess');

// Manager related schedule api + controller
Route::post('/manager/create/schedule', 'Manager\ExternalAPiController@createScheduleProcess');
Route::post('/manager/view/schedule', 'Manager\ExternalAPiController@getScheduleProcess');
Route::post('/manager/edit/schedule', 'Manager\ExternalAPiController@editScheduleProcess');
Route::post('/manager/delete/schedule', 'Manager\ExternalAPiController@deleteScheduleProcess');

//Client
Route::post('/client/userListing/admin' , 'client\ClientAPIController@userListingAdmin');
Route::post('/client/userListing/agent' , 'client\ClientAPIController@userListingAgent');
Route::post('/client/userListing/manager' , 'client\ClientAPIController@userListingManager');
Route::post('/client/userListing/superuser' , 'client\ClientAPIController@userListingSuperuser');
Route::post('/client/userListing/rootuser' , 'client\ClientAPIController@userListingRootuser');

Route::post('/client/userDetail/admin' , 'client\ClientAPIController@userDetailAdmin');
Route::post('/client/userDetail/agent' , 'client\ClientAPIController@userDetailAgent');
Route::post('/client/userDetail/manager' , 'client\ClientAPIController@userDetailManager');
Route::post('/client/userDetail/superuser' , 'client\ClientAPIController@userDetailSuperuser');
Route::post('/client/userDetail/rootuser' , 'client\ClientAPIController@userDetailRootuser');

Route::post('/client/addUser/admin' , 'client\ClientAPIController@addUserAdmin');
Route::post('/client/addUser/agent' , 'client\ClientAPIController@addUserAgent');
Route::post('/client/addUser/manager' , 'client\ClientAPIController@addUserManager');
Route::post('/client/addUser/superuser' , 'client\ClientAPIController@addUserSuperuser');
Route::post('/client/addUser/rootuser' , 'client\ClientAPIController@addUserRootuser');

Route::post('/client/editUser/admin' , 'client\ClientAPIController@editUserAdmin');
Route::post('/client/editUser/agent' , 'client\ClientAPIController@editUserAgent');
Route::post('/client/editUser/manager' , 'client\ClientAPIController@editUserManager');
Route::post('/client/editUser/superuser' , 'client\ClientAPIController@editUserSuperuser');
Route::post('/client/editUser/rootuser' , 'client\ClientAPIController@editUserRootuser');

Route::post('/client/deleteUser/admin' , 'client\ClientAPIController@deleteUserAdmin');
Route::post('/client/deleteUser/superuser' , 'client\ClientAPIController@deleteUserSuperuser');
Route::post('/client/deleteUser/rootuser' , 'client\ClientAPIController@deleteUserRootuser');

Route::post('/client/suspendUser/admin' , 'client\ClientAPIController@suspendUserAdmin');
Route::post('/client/suspendUser/superuser' , 'client\ClientAPIController@suspendUserSuperuser');
Route::post('/client/suspendUser/rootuser' , 'client\ClientAPIController@suspendUserRootuser');

//Buyer
Route::post('/buyer/buyerListing/admin' , 'buyer\BuyerAPIController@buyerListingAdmin');
Route::post('/buyer/buyerDetail/admin' , 'buyer\BuyerAPIController@buyerDetailAdmin');
Route::post('/buyer/addBuyer/admin' , 'buyer\BuyerAPIController@addBuyerAdmin');
Route::post('/buyer/editBuyer/admin' , 'buyer\BuyerAPIController@editBuyerAdmin');
Route::post('/buyer/deleteBuyer/admin' , 'buyer\BuyerAPIController@deleteBuyerAdmin');
Route::post('/buyer/approveBuyer/admin' , 'buyer\BuyerAPIController@approveBuyerAdmin');
Route::post('/buyer/rejectBuyer/admin' , 'buyer\BuyerAPIController@rejectBuyerAdmin');
Route::post('/buyer/suspendBuyer/admin' , 'buyer\BuyerAPIController@suspendBuyerAdmin');

//Seller
Route::post('/seller/sellerListing/admin' , 'seller\SellerAPIController@sellerListingAdmin');
Route::post('/seller/sellerDetail/admin' , 'seller\SellerAPIController@sellerDetailAdmin');
Route::post('/seller/addSeller/admin' , 'seller\SellerAPIController@addSellerAdmin');
Route::post('/seller/editSeller/admin' , 'seller\SellerAPIController@editSellerAdmin');
Route::post('/seller/deleteSeller/admin' , 'seller\SellerAPIController@deleteSellerAdmin');
Route::post('/seller/approveSeller/admin' , 'seller\SellerAPIController@approveSellerAdmin');
Route::post('/seller/rejectSeller/admin' , 'seller\SellerAPIController@rejectSellerAdmin');
// Route::post('/seller/suspendSeller/admin' , 'seller\SellerAPIController@suspendSellerAdmin');

/// External APi
// 1. insert, update, remove agent / manager
// 2. to allow admin to view agent / manager details
// 3. to allow manager to edit agent work details
// 4. allow agent to add, edit, view schedule / remark
// 5. allow manager / admin to view agent schedule
