<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Login Agent
Route::get('/agent/login', 'Agent\LoginController@index');
Route::post('/agent/login', 'Agent\LoginController@agentLoginProcess');

// Dashboard Agent
Route::get('/agent/dashboard', 'Agent\DashboardController@index');

// Logout Agent
Route::get('/agent/logout', 'Agent\LogoutController@agentLogoutProcess');

// Property Matcher
Route::get('/agent/matcher/form', 'Agent\PropertyMatchFormController@index');
Route::post('/agent/matcher/form', 'Agent\PropertyMatchFormController@insertPatternProcess');

// Property Match List
Route::get('/agent/matcher/list', 'Agent\PropertyMatchListController@index');
Route::post('/agent/matcher/delete', 'Agent\PropertyMatchListController@removePatternProcess');

// Adding Remarks
Route::post('/agent/remark', 'Agent\LogController@insertLogProcess');