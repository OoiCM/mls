<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// //Admin
// Route::view('/login', 'administrator.admin_login');
// Route::get('/show/{id}', 'administrator\AdminController@show'); //shd use datatable
// Route::get('/show_all', 'administrator\AdminController@index'); //shd use datatable
// Route::get('/create/form/{id}','administrator\AdminController@createForm')->name('admin.form');//->middleware('auth')
// Route::get('/edit/form/{id}', 'administrator\AdminController@editForm');
// Route::get('/delete/form/{id}', 'administrator\AdminController@deleteForm');
// Route::post('/login', 'administrator\LoginController@admin_login')->name('admin.login');
// Route::post('/create/{id}', 'administrator\AdminController@store')->name('admin.store');
// Route::post('/edit/{id}', 'administrator\AdminController@update');
// Route::post('/delete/admin/{id}', 'administrator\AdminController@destroy');

// // //Property
// // Route::view('/property/form', 'property_form');
// // Route::get('/property/agent_reserved_property_form', 'PropertyController@agent_reserved_property_form');
// // Route::post('/property/create', 'PropertyController@store')->name('property.store');
// // Route::post('/property/sell/update_agent_id', 'PropertyController@update_agent_id');


// //home dashboard page
// Route::view('/home', 'admin_home')->name('admin_home');

