<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// //Superuser
// Route::view('/superuser/login', 'superuser_login');
// Route::get('/superuser/show/{id}', 'SuperuserController@show'); //shd use datatable
// Route::get('/superuser/show_all', 'SuperuserController@index'); //shd use datatable
// Route::get('/superuser/create/form/{id}','SuperuserController@createForm')->name('superuser.form');//->middleware('auth')
// Route::get('/superuser/edit/form/{id}', 'SuperuserController@editForm');
// Route::get('/superuser/delete/form/{id}', 'SuperuserController@deleteForm');
// Route::post('/superuser/login', 'Auth\LoginController@superuser_login')->name('superuser.login');
// Route::post('/superuser/create/{id}', 'SuperuserController@store')->name('superuser.store');
// Route::post('/superuser/edit/{id}', 'SuperuserController@update');
// Route::post('/superuser/delete/superuser/{id}', 'SuperuserController@destroy');

// //home dashboard page
// Route::view('/superuser/home', 'superuser_home')->name('superuser_home');

