<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// //Root
// Route::view('/root/login', 'root_login');
// Route::get('/root/show/{id}', 'RootController@show'); //shd use datatable
// Route::get('/root/show_all', 'RootController@index'); //shd use datatable
// Route::get('/root/create/form/{id}','RootController@createForm')->name('root.form');//->middleware('auth')
// Route::get('/root/edit/form/{id}', 'RootController@editForm');
// Route::get('/root/delete/form/{id}', 'RootController@deleteForm');
// Route::post('/root/login', 'Auth\LoginController@root_login')->name('root.login');
// Route::post('/root/create/{id}', 'RootController@store')->name('root.store');
// Route::post('/root/edit/{id}', 'RootController@update');
// Route::post('/root/delete/root/{id}', 'RootController@destroy');

// //home dashboard page
// Route::view('/root/home', 'root_home')->name('root_home');

