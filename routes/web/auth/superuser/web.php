<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Superuser
Route::view('/login', 'superuser.superuser_login');
Route::get('/show/{id}', 'SuperuserController@show'); //shd use datatable
Route::get('/show_all', 'SuperuserController@index'); //shd use datatable
Route::get('/create/form/{id}','SuperuserController@createForm')->name('superuser.form');//->middleware('auth')
Route::get('/edit/form/{id}', 'SuperuserController@editForm');
Route::get('/delete/form/{id}', 'SuperuserController@deleteForm');
Route::post('/login', 'LoginController@superuser_login')->name('superuser.login');
Route::post('/create/{id}', 'SuperuserController@store')->name('superuser.store');
Route::post('/edit/{id}', 'SuperuserController@update');
Route::post('/delete/superuser/{id}', 'SuperuserController@destroy');

//home dashboard page
Route::view('/superuser/home', 'superuser.superuser_home')->name('superuser_home');

