<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin
Route::get('/login', 'AdminController@login_page');
Route::get('/show/{id}', 'AdminController@show'); //shd use datatable
Route::get('/show_all', 'AdminController@index'); //shd use datatable
Route::get('/create/form/{id}','AdminController@createForm')->name('admin.form');//->middleware('auth')
Route::get('/edit/form/{id}', 'AdminController@editForm');
Route::get('/delete/form/{id}', 'AdminController@deleteForm');
Route::post('/login', 'LoginController@admin_login')->name('admin.login');
Route::post('/create/{id}', 'AdminController@store')->name('admin.store');
Route::post('/edit/{id}', 'AdminController@update');
Route::post('/delete/admin/{id}', 'AdminController@destroy');

//Property
Route::get('/create/property_form', 'PropertyController@create_property_form');
Route::get('/view/property_menu/{id}', 'PropertyController@index');
Route::get('/agent_reserved_property_form', 'PropertyController@agent_reserved_property_form');
Route::post('/edit/property_form/{id}', 'PropertyController@edit_property_form');
Route::get('/delete/property_form/{id}', 'PropertyController@delete_property_form');
Route::get('/edit/property_menu/{id}', 'PropertyController@edit_property_menu');
Route::post('/view/property', 'PropertyController@show');
Route::post('/property/create', 'PropertyController@store')->name('property.store');
Route::post('/property/sell/update_agent_id', 'PropertyController@update_agent_id');
Route::post('/property/edit', 'PropertyController@edit');
Route::post('/property/delete/{id}', 'PropertyController@destroy');

//Client (CS side)
Route::get('/get_all_client', 'ClientController@index')->name('get_all_client');
Route::get('/create_client_form', 'ClientController@create_client_form');
Route::post('/create_client', 'ClientController@store');
Route::get('/get_client', 'ClientController@show');
Route::post('/edit_client', 'ClientController@edit');
Route::post('/delete_client', 'ClientController@destroy');

//Buyer (CS side)
Route::get('/get_all_buyer', 'BuyerController@index')->name('get_all_buyer');
Route::get('/create_buyer_form', 'BuyerController@create_buyer_form');
Route::get('/create_buyer', 'BuyerController@store');
Route::get('/get_buyer', 'BuyerController@show');
Route::get('/edit_buyer', 'BuyerController@edit');
Route::get('/delete_buyer', 'BuyerController@destroy');

//Seller (CS side)
Route::get('/get_all_seller', 'SellerController@index')->name('get_all_seller');
Route::get('/create_seller_form', 'SellerController@create_seller_form');
Route::get('/create_seller', 'SellerController@store');
Route::get('/get_seller', 'SellerController@show');
Route::get('/edit_seller', 'SellerController@edit');
Route::get('/delete_seller', 'SellerController@destroy');

//Approval (CS side)
Route::get('/manage_buyer_request', 'ApprovalController@buyer_index')->name('get_all_buyer_request');
Route::get('/manage_seller_request', 'ApprovalController@seller_index')->name('get_all_seller_request');
Route::get('/approve_buyer_request', 'ApprovalController@approve_buyer_request');
Route::get('/approve_seller_request', 'ApprovalController@approve_seller_request');
Route::get('/reject_buyer_request', 'ApprovalController@reject_buyer_request');
Route::get('/reject_seller_request', 'ApprovalController@reject_seller_request');
Route::get('/suspend_buyer_request', 'ApprovalController@suspend_buyer_request');
Route::get('/suspend_seller_request', 'ApprovalController@suspend_seller_request');

//Manager (CM side)
Route::get('/get_all_manager', 'ManagerController@index')->name('get_all_manager');
Route::get('/create_manager_form', 'ManagerController@create_manager_form');
Route::get('/create_manager', 'ManagerController@store');
Route::get('/get_manager', 'ManagerController@show');
Route::get('/edit_manager', 'ManagerController@edit');
Route::get('/delete_manager', 'ManagerController@destroy');

//Agent (CM side)
Route::get('/get_all_agent', 'AgentController@index')->name('get_all_agent');
Route::get('/create_agent_form', 'AgentController@create_agent_form');
Route::get('/create_agent', 'AgentController@store');
Route::get('/get_agent', 'AgentController@show');
Route::get('/edit_agent', 'AgentController@edit');
Route::get('/delete_agent', 'AgentController@destroy');

//home dashboard page
// Route::view('/home', 'administrator.admin_home')->name('admin_home');
Route::get('/home/{id}', 'AdminController@home')->name('admin_home');

