<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Root
Route::view('/login', 'root.root_login');
Route::get('/show/{id}', 'RootController@show'); //shd use datatable
Route::get('/show_all', 'RootController@index'); //shd use datatable
Route::get('/create/form/{id}','RootController@createForm')->name('root.form');//->middleware('auth')
Route::get('/edit/form/{id}', 'RootController@editForm');
Route::get('/delete/form/{id}', 'RootController@deleteForm');
Route::post('/login', 'LoginController@root_login')->name('root.login');
Route::post('/create/{id}', 'RootController@store')->name('root.store');
Route::post('/edit/{id}', 'RootController@update');
Route::post('/delete/root/{id}', 'RootController@destroy');

//home dashboard page
Route::view('/home', 'root.root_home')->name('root_home');

