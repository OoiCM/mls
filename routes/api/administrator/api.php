<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/show_all', function (Request $request) {
//     return "lala";
// })->name('show_all_client');

// Route::middleware('auth:api')->get('/show_all', 'AdminController@index');
// Route::get('/show_all_properties', 'AdminController@index'); //without token validation


//create property
// Route::get('/create_property_form', 'PropertyController@api_create_property_form'); //deprecated
Route::post('/manager/create_property', 'PropertyController@api_manager_store');
Route::post('/agent/create_property', 'PropertyController@api_agent_store');
Route::post('/client/create_property', 'PropertyController@api_client_store');

//delete property
// Route::get('/delete_property_form', 'PropertyController@api_delete_property_form'); //deprecated
Route::post('/manager/delete_property', 'PropertyController@api_manager_destroy');
Route::post('/agent/delete_property', 'PropertyController@api_agent_destroy');
Route::post('/client/delete_property', 'PropertyController@api_client_destroy');

//update/edit property
// Route::get('/reserved_property_form', 'PropertyController@api_reserved_property_form'); //deprecated
Route::post('/manager/reserved_property', 'PropertyController@api_manager_reserved_property');
Route::post('/agent/reserved_property', 'PropertyController@api_agent_reserved_property');
// Route::get('/edit_property_menu', 'PropertyController@api_edit_property_menu');  //deprecated
// Route::get('/edit_property_form/{id}', 'PropertyController@api_edit_property_form'); //deprecated
Route::post('/manager/edit_property', 'PropertyController@api_manager_edit');
Route::post('/agent/edit_property', 'PropertyController@api_agent_edit');
Route::post('/client/edit_property', 'PropertyController@api_client_edit');

//edit property_agent_id for manager
Route::post('/manager/update_property_agent', 'PropertyController@api_manager_update_property_agent_id');

//retrieve property
// Route::get('/view_property_menu', 'PropertyController@api_index'); //deprecated
Route::post('/manager/view_property', 'PropertyController@api_manager_show');
Route::post('/agent/view_property', 'PropertyController@api_agent_show');
Route::post('/client/view_property', 'PropertyController@api_client_show');


//seek approval request
Route::post('/client/approval_request', 'ApprovalController@api_client_new_approval');


// Route::middleware('auth:api')->get('/update_agent_id', 'PropertyController@update_agent_id');

