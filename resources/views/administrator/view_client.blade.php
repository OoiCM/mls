<html>
	<form method="post" action="/admin/auth/edit_client">
		@csrf
		<h1>View/Edit Client</h1>
		@if (isset($errors))
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
		@endif

		<input type="button" value="Edit">

		<input type="hidden" value="{{$data->client_token}}">

		<div class="row">
			<div class="col-md-12">
				<label>ID : </label> <input type="text" name="client_id" value="{{isset($data->client_id)}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Email : </label> <input type="text" name="email" value="{{$data->email}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>First Name : </label> <input type="text" name="firstname" value="{{$data->firstname}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Last Name : </label> <input type="text" name="lastname" value="{{$data->lastname}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Nickname: </label> <input type="text" name="nickname" value="{{$data->nickname}}" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Home Contact: </label> <input type="text" name="home_contact" value="{{$data->home_contact}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Mobile : </label> <input type="text" name="mobile" value="{{$data->mobile}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Alternate Contact : </label> <input type="text" name="alt_contact" value="{{$data->alt_contact}}" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Address 1 : </label> <input type="text" name="address_1" value="{{$data->address_1}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Address 2 : </label> <input type="text" name="address_2" value="{{$data->address_2}}" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Account Status : </label> <input type="text" name="account_status" value="{{$data->account_status}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Created at : </label> <input type="text" name="created_at" value="{{$data->created_at}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Updated at : </label> <input type="text" name="updated_at" value="{{$data->updated_at}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Suspend by : </label> <input type="text" name="suspend_by" value="{{$data->suspend_by}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Suspend date : </label> <input type="text" name="suspend_date" value="{{$data->suspend_date}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Deleted by : </label> <input type="text" name="deleted_by" value="{{$data->deleted_by}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Deleted status : </label> <input type="text" name="deleted_status" value="{{$data->deleted_status}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Deleted at : </label> <input type="text" name="deleted_at" value="{{$data->deleted_at}}" required="true" readonly="true">
			</div>
		</div>

		<input type="submit" value="Submit">
	</div>
</html>
<script>
	//click on edit button to edit
</script>