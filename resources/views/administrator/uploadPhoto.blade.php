<html>
    <form method="post" action="/property/upload/photo" enctype="multipart/form-data">
        @csrf
        <h1>Upload Photo</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <label>Filename : </label> <input type="text" name="filename"">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>File : </label> <input type="file" name="photo" accept="image/jpeg, image/png"/>
            </div>
        </div>
        <input type="submit" value="Submit">
    </div>
</html>