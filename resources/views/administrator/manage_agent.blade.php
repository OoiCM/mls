<html>
	<form method="post" action="{{$route}}">
		@csrf
		<h1>Manage Agent</h1>
		<div class="row">
			<div class="col-md-12">
				<label>ID : </label> 
				<select name='id'>
				@foreach ($properties as $property)
					<option value={{$property['id']}}>{{$property['id']}}</option>
				@endforeach
				</select>
			</div>
		</div>
		<input type="submit" value="Submit">
	</div>
</html>