<html>
	<form method="post" action="/admin/auth/delete/admin/{{$id}}">
		@csrf
		<h1>Delete Admin</h1>
		<div class="row">
			<div class="col-md-12">
				<label>ID : </label> 
				<select name='id'>
				@foreach ($admins as $admin)
					<option value={{$admin['id']}}>{{$admin['id']}}</option>
				@endforeach
				</select>
			</div>
		</div>
		<input type="submit" value="Submit">
	</div>
</html>