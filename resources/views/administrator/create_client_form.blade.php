<html>
	<form method="post" action="/admin/auth/create_client">
		@csrf
		<h1>Create New Client</h1>

		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		<div class="row">
			<div class="col-md-12">
				<label>Email : </label> <input type="text" name="email" value="{{ old('email') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Password : </label> <input type="password" name="password" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Retype Password : </label> <input type="password" name="retype_password" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>First Name : </label> <input type="text" name="firstname" value="{{ old('firstname') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Last Name : </label> <input type="text" name="lastname" value="{{ old('lastname') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Nickname: </label> <input type="text" name="nickname" value="{{ old('nickname') }}">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<label>Home Contact: </label> <input type="text" name="home_contact" value="{{ old('home_contact') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Mobile : </label> <input type="text" name="mobile" value="{{ old('mobile') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Alternate Contact : </label> <input type="text" name="alt_contact" value="{{ old('alt_contact') }}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Address 1 : </label> <input type="text" name="address_1" value="{{ old('address_1') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Address 2 : </label> <input type="text" name="address_2" value="{{ old('address_2') }}">
			</div>
		</div>
		<input type="submit" value="Submit">
	</div>
</html>