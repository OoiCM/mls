<html>
	<form method="post" action="/admin/auth/edit/{{$id}}">
		@csrf
		<h1>Edit Admin</h1>

		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		<div class="row">
			<div class="col-md-12">
				<label>ID : </label> <input type="text" name="id" value="{{$id}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Username : </label> <input type="text" name="username" value="{{$username}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Old Password : </label> <input type="password" name="old_password">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>New Password : </label> <input type="password" name="new_password">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Retype New Password : </label> <input type="password" name="new_retype_password">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Status : </label> <input type="text" name="status" value="{{$status}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Permission : </label> <input type="text" name="permission" value="{{$permission}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Is delete : </label> <input type="text" name="is_delete" value="{{$is_delete}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Last login : </label> <input type="text" name="last_login" value="{{$last_login}}" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>First Name : </label> <input type="text" name="first_name" value="{{$first_name}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Last Name : </label> <input type="text" name="last_name" value="{{$last_name}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Contact : </label> <input type="text" name="contact" value="{{$contact}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Email : </label> <input type="text" name="email" value="{{$email}}">
			</div>
		</div>
		<input type="submit" value="Submit">
	</div>
</html>