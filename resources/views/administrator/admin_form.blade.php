<html>
	<form method="post" action="/admin/auth/create/{{$id}}">
		@csrf
		<h1>Create New Admin</h1>

		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		<div class="row">
			<div class="col-md-12">
				<label>Username : </label> <input type="text" name="username" value="{{ old('username') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Password : </label> <input type="password" name="password" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Retype Password : </label> <input type="password" name="retype_password" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Status : </label> <input type="text" name="status" value="1" readonly>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Permission : </label> <input type="text" name="permission" value="{{ old('permission') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>First Name : </label> <input type="text" name="first_name" value="{{ old('first_name') }}" required="true">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<label>Last Name : </label> <input type="text" name="last_name" value="{{ old('last_name') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Contact : </label> <input type="text" name="contact" value="{{ old('contact') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Email : </label> <input type="text" name="email" value="{{ old('email') }}" required="true">
			</div>
		</div>
		<input type="submit" value="Submit">
	</div>
</html>