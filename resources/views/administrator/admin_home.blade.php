@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul>
                        <li>Manage Admin
                            <ul>
                                <li><a href="http://127.0.0.1:8000/admin/auth/show/{{$id}}">View Admin</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/show_all">View All Admin</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/create/form/{{$id}}">Create Admin</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/edit/form/{{$id}}">Edit Admin</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/delete/form/{{$id}}">Delete Admin</li>
                            </ul>
                        </li>
                    </ul>

                    <ul>
                        <li>Manage Property
                            <ul>
                                <!-- <li><a href="http://127.0.0.1:8000/admin/auth/view/property">View Property</li> -->
                                <li><a href="http://127.0.0.1:8000/admin/auth/view/property_menu/{{$id}}">View All Property</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/agent_reserved_property_form">Agent Reserve Property Form</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/create/property_form">Create Property</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/edit/property_menu/{{$id}}">Edit Property</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/delete/property_form/{{$id}}">Delete Property</li>
                            </ul>
                        </li>
                    </ul>

                    <ul>
                        <li>Manage Client
                            <ul>
                                <li><a href="">View Client</li>
                                <li><a href="http://127.0.0.1:8000/admin/auth/client/view_all_client">View All Client</li>
                                <li><a href="">Create Client</li>
                                <li><a href="">Edit Client</li>
                                <li><a href="">Delete Client</li>
                            </ul>
                        </li>
                    </ul>

                    <ul>
                        <li>Manage Agent
                            <ul>
                                <li><a href="">View Agent</li>
                                <li><a href="">View All Agent</li>
                                <li><a href="">Create Agent</li>
                                <li><a href="">Edit Agent</li>
                                <li><a href="">Delete Agent</li>
                            </ul>
                        </li>
                    </ul>

                    <ul>
                        <li>Manage Manager
                            <ul>
                                <li><a href="">View Manager</li>
                                <li><a href="">View All Manager</li>
                                <li><a href="">Create Manager</li>
                                <li><a href="">Edit Manager</li>
                                <li><a href="">Delete Manager</li>
                            </ul>
                        </li>
                    </ul>

                    <ul>
                        <li>Manage Buyer
                            <ul>
                                <li><a href="">View Buyer</li>
                                <li><a href="">View All Buyer</li>
                                <li><a href="">Create Buyer</li>
                                <li><a href="">Edit Buyer</li>
                                <li><a href="">Delete Buyer</li>
                            </ul>
                        </li>
                    </ul>

                    <ul>
                        <li>Manage Seller
                            <ul>
                                <li><a href="">View Seller</li>
                                <li><a href="">View All Seller</li>
                                <li><a href="">Create Seller</li>
                                <li><a href="">Edit Seller</li>
                                <li><a href="">Delete Seller</li>
                            </ul>
                        </li>
                    </ul>

                    <ul>
                        <li>Manage Approval
                            <ul>
                                <li><a href="">View Approval</li>
                                <li><a href="">View All Approval</li>
                                <li><a href="">Create Approval</li>
                                <li><a href="">Edit Approval</li>
                                <li><a href="">Delete Approval</li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
