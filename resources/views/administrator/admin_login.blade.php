<html>
    <form method="post" action="/admin/auth/login">
        @csrf
        <h1>Admin Login</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <label>Username : </label> <input type="text" name="username" value="{{ old('username') }}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Password : </label> <input type="password" name="password">
            </div>
        </div>
        <!-- remember me - pending -->
        <input type="submit" value="Submit">
    </div>
</html>