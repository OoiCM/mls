<html>
	<form method="post" action="/admin/auth/create_agent">
		@csrf
		<h1>Create New Agent</h1>

		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		<div class="row">
			<div class="col-md-12">
				<label>Email : </label> <input type="text" name="email" value="{{ old('email') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Password : </label> <input type="password" name="password" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Retype Password : </label> <input type="password" name="retype_password" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Username : </label> <input type="text" name="username" value="{{ old('username') }}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Display Name : </label> <input type="text" name="display_name" value="{{ old('display_name') }}" required="true">
			</div>
		</div>
		
		<input type="submit" value="Submit">
	</div>
</html>