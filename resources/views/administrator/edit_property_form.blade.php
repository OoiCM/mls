<html>
	<form method="post" action="{{$route}}">
		@csrf
		<h1>Add Property</h1>
		@if (isset($errors))
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
		@endif

		<div class="row">
			<div class="col-md-12">
				<label>ID : </label> <input type="text" name="id" value="{{$id}}" required="true" readonly>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Unit No : </label> <input type="text" name="unit_no" value="{{$unit_no}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Floor : </label> <input type="text" name="floor" value="{{$floor}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Block : </label> <input type="text" name="block" value="{{$block}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Building : </label> <input type="text" name="building" value="{{$building}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Street : </label> <input type="text" name="street" value="{{$street}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>District : </label> <input type="text" name="district" value="{{$district}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Property Type : </label> <input type="text" name="property_type" value="{{$property_type}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Property Usage : </label> <input type="text" name="property_usage" value="{{$property_usage}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Price : </label> <input type="text" name="price" value="{{$price}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Gross Area : </label> <input type="text" name="gross_area" value="{{$gross_area}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Net Area : </label> <input type="text" name="net_area" value="{{$net_area}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Sale Price Per Sqft : </label> <input type="text" name="sale_price_per_sqft" value="{{$sale_price_per_sqft}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Latlon : </label> <input type="text" name="latlon" value="{{$latlon}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Latitude : </label> <input type="text" name="latitude" value="{{$latitude}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Longitude : </label> <input type="text" name="longitude" value="{{$longitude}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Building Complete Date : </label> <input type="text" name="building_complete_date" value="{{$building_complete_date}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Mortgage Start Date : </label> <input type="text" name="mortgage_start_date" value="{{$mortgage_end_date}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Mortgage End Date : </label> <input type="text" name="mortgage_end_date" value="{{$mortgage_end_date}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Facing View : </label> <input type="text" name="facing_view" value="{{$facing_view}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Room : </label> <input type="text" name="room" value="{{$room}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Bathroom : </label> <input type="text" name="bathroom" value="{{$bathroom}}" required="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Decoration : </label> <input type="text" name="decoration" value="{{$decoration}}"">
			</div>
		</div>

		<input type="submit" value="Submit">
	</div>
</html>