<html>
	<form method="post" action="/admin/auth/edit_agent">
		@csrf
		<h1>View/Edit Agent</h1>
		@if (isset($errors))
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
		@endif

		<input type="button" value="Edit">

		<div class="row">
			<div class="col-md-12">
				<label>ID : </label> <input type="text" name="client_id" value="{{isset($data->manager_id)}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Username : </label> <input type="text" name="username" value="{{$data->username}}" required="true" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Email : </label> <input type="text" name="email" value="{{$data->email}}" required="true">
			</div>
		</div>
		<!-- <div class="row">
			<div class="col-md-12">
				<label>Password : </label> <input type="text" name="password" value="{{$data->password}}" required="true" readonly="true">
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<label>Display Name: </label> <input type="text" name="display_name" value="{{$data->display_name}}" readonly="true">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label>Created By : </label> <input type="text" name="created_by" value="{{$data->created_by}}" required="true" readonly="true">
			</div>
		</div>

		<input type="submit" value="Submit">
	</div>
</html>
<script>
	//click on edit button to edit
</script>