<html>
	<form method="post" action="/admin/auth/property/sell/update_agent_id">
		@csrf
		<h1>Reserved Property</h1>
		<div class="row">
			<div class="col-md-12">
				<label>Property ID : </label> 
				<select name='property_id'>
				@foreach ($properties as $property)
					<option value={{$property['id']}}>{{$property['id']}}</option>
				@endforeach
				</select>
			</div>
		</div>
		<div class="row">
            <div class="col-md-12">
                <label>Agent ID : </label> <input type="text" name="agent_id">
            </div>
        </div>
		<input type="submit" value="Submit">
	</div>
</html>