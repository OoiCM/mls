@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul>
                        <li>Manage Root
                            <ul>
                                <li><a href="http://127.0.0.1:8000/root/auth/show/{{$id}}">View Root</li>
                                <li><a href="http://127.0.0.1:8000/root/auth/show_all">View All Root</li>
                                <li><a href="http://127.0.0.1:8000/root/auth/create/form/{{$id}}">Create Root</li>
                                <li><a href="http://127.0.0.1:8000/root/auth/edit/form/{{$id}}">Edit Root</li>
                                <li><a href="http://127.0.0.1:8000/root/auth/delete/form/{{$id}}">Delete Root</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
