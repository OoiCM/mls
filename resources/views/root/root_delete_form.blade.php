<html>
	<form method="post" action="/root/auth/delete/root/{{$id}}">
		@csrf
		<h1>Delete Root</h1>
		<div class="row">
			<div class="col-md-12">
				<label>ID : </label> 
				<select name='id'>
				@foreach ($roots as $root)
					<option value={{$root['id']}}>{{$root['id']}}</option>
				@endforeach
				</select>
			</div>
		</div>
		<input type="submit" value="Submit">
	</div>
</html>