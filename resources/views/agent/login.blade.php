<!DOCTYPE html>
<html lang="en" class="bg-dark">
	<head>
		<meta charset="utf-8" />
		<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		
		<title>Panel | Login</title>
			
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ URL::asset('css/font.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" type="text/css" />
		
		<!--[if lt IE 9]>
			<script src="{{ URL::asset('js/ie/html5shiv.js') }}"></script>
			<script src="{{ URL::asset('js/ie/respond.min.js') }}"></script>
			<script src="{{ URL::asset('js/ie/excanvas.js') }}"></script>
		<![endif]-->
	</head>
	<body>
		<section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
			<div class="container aside-xxl">
				<section class="panel panel-default bg-white m-t-lg">
					<header class="panel-heading text-center">
						<strong>Sign in</strong>
					</header>
					<form id="loginAgent" action="/agent/login" method="post" class="panel-body wrapper-lg">
						<!--{{csrf_field()}}-->
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<label class="control-label">ID</label>
							<input type="text" id="id" name="id" placeholder="A0001" class="form-control input-lg" required>
						</div>
						<div class="form-group">
							<label class="control-label">Password</label>
							<input type="password" id="password" name="password" placeholder="Password" class="form-control input-lg" required>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox"> Keep me logged in
							</label>
						</div>
						<div class="line line-dashed"></div>
						<a href="#" class="pull-right m-t-xs"><small>Forgot password?</small></a>
						<button type="submit" class="btn btn-primary">Sign in</button>
					</form>
				</section>
			</div>
		</section>
		<!-- footer -->
		<footer id="footer">
			<div class="text-center padder">
				<p>
					<small>CM<br>&copy; 2018-18-18</small>
				</p>
			</div>
		</footer>
		<!-- / footer -->
		
		<!-- <script src="{{ URL::asset('js/jquery.min.js') }}"></script>-->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<!-- Bootstrap -->
		<script src="{{ URL::asset('js/bootstrap.js') }}"></script>
		<!-- App -->
		<script src="{{ URL::asset('js/app.js') }}"></script>
		<script src="{{ URL::asset('js/app.plugin.js') }}"></script>
		<script src="{{ URL::asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>

	</body>
</html>
