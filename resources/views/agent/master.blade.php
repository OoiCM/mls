<!DOCTYPE html>
<html lang="en" class="app">
      <head>
            <meta charset="utf-8" />
            <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
            <meta name="csrf-token" content="{{ csrf_token() }}" />
            
            <title>Panel | @yield('title')</title>
                  
            <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />
            <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" type="text/css" />
            <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}" type="text/css" />
            <link rel="stylesheet" href="{{ URL::asset('css/font.css') }}" type="text/css" />
            <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" type="text/css" />
            <link rel="stylesheet" href="{{ URL::asset('js/fuelux/fuelux.css') }}" type="text/css" />
            
            <!--[if lt IE 9]>
                  <script src="{{ URL::asset('js/ie/html5shiv.js') }}"></script>
                  <script src="{{ URL::asset('js/ie/respond.min.js') }}"></script>
                  <script src="{{ URL::asset('js/ie/excanvas.js') }}"></script>
            <![endif]-->
                  
      </head>
      <body>
            <section class="vbox">
                  <header class="bg-dark dk header navbar navbar-fixed-top-xs">
                        <div class="navbar-header aside-md">
                              <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
                                    <i class="fa fa-bars"></i>
                              </a>
                              <a href="#" class="navbar-brand" data-toggle="fullscreen">MLS</a>
                              <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
                                    <i class="fa fa-cog"></i>
                              </a>
                        </div>
                        <ul class="nav navbar-nav hidden-xs">
                              <li class="dropdown">
                                    <a href="#" class="dropdown-toggle dker" data-toggle="dropdown">
                                          <i class="fa fa-bars"></i> 
                                          <span class="font-bold">Profile</span>
                                    </a>
                                    <section class="dropdown-menu aside-xl on animated fadeInLeft no-borders lt">
                                          <div class="wrapper lter m-t-n-xs">
                                                <div class="clear">
                                                      <a href="#"><span class="text-white font-bold">{{ $display_name }}</a></span>
                                                      <small class="block">Art Director</small>
                                                </div>
                                          </div>
                                          <div class="row m-l-none m-r-none m-b-n-xs text-center">
                                                <div class="col-xs-4">
                                                      <div class="padder-v">
                                                            <span class="m-b-xs h4 block text-white">245</span>
                                                            <small class="text-muted">Followers</small>
                                                      </div>
                                                </div>
                                                <div class="col-xs-4 dk">
                                                      <div class="padder-v">
                                                            <span class="m-b-xs h4 block text-white">55</span>
                                                            <small class="text-muted">Likes</small>
                                                      </div>
                                                </div>
                                                <div class="col-xs-4">
                                                      <div class="padder-v">
                                                            <span class="m-b-xs h4 block text-white">2,035</span>
                                                            <small class="text-muted">Photos</small>
                                                      </div>
                                                </div>
                                          </div>
                                    </section>
                              </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">
                              <li class="dropdown hidden-xs">
                                    <a href="#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-plus"></i></a>
                                    <section class="dropdown-menu aside-xl animated fadeInUp">
                                          <section class="panel bg-white">
                                                <form role="search">
                                                      <div class="form-group wrapper m-b-none">
                                                            <div class="input-group">
                                                                  <input type="text" id="log" name="log" class="form-control" placeholder="Add Remarks">
                                                                  <span class="input-group-btn">
                                                                        <button type="button" class="btn btn-info btn-icon" onclick="insertLog();">
                                                                              <i class="fa fa-plus"></i>
                                                                        </button>
                                                                  </span>
                                                            </div>
                                                      </div>
                                                </form>
                                          </section>
                                    </section>
                              </li>
                              <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                          {{ $display_name }}<b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu animated fadeInRight">
                                          <span class="arrow top"></span>
                                          <li>
                                                <a href="#">Settings</a>
                                          </li>
                                          <li>
                                                <a href="profile.html">Profile</a>
                                          </li>
                                          <li>
                                                <a href="#">
                                                      <span class="badge bg-danger pull-right">100+</span>Notifications
                                                </a>
                                          </li>
                                          <li>
                                                <a href="docs.html">Help</a>
                                          </li>
                                          <li class="divider"></li>
                                          <li>
                                                <a href="/agent/logout" >Logout</a>
                                          </li>
                                    </ul>
                              </li>      
                        </ul>  
                  </header>
                  <section>
                        <section class="hbox stretch">
                              <!-- .aside -->
                              <aside class="bg-dark lter aside-md hidden-print" id="nav">
                              <section class="vbox">
                                    <section class="w-f scrollable">
                                          <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
                                                <!-- nav -->
                                                <nav class="nav-primary hidden-xs">
                                                      <ul class="nav">
                                                            <li class="{{ $dashboard_active }}">
                                                                  <a href="/agent/dashboard" class="{{ $dashboard_active }}">
                                                                        <i class="fa fa-th-large icon">
                                                                              <b class="bg-danger"></b>
                                                                        </i>
                                                                        <span>Index</span>
                                                                  </a>
                                                            </li>
                                                            <li class="{{ $matcher_active }}">
                                                                  <a href="#pattern_matcher" class="{{ $matcher_active }}">
                                                                        <i class="fa fa-search icon">
                                                                              <b class="bg-primary dker"></b>
                                                                        </i>
                                                                        <span class="pull-right">
                                                                              <i class="fa fa-angle-down text"></i>
                                                                              <i class="fa fa-angle-up text-active"></i>
                                                                        </span>
                                                                        <span>Pattern Matcher</span>
                                                                  </a>
                                                                  <ul class="nav lt">
                                                                        <li class="{{ $matcher_form_active }}">
                                                                              <a href="/agent/matcher/form" class="{{ $matcher_form_active }}">
                                                                                    <i class="fa fa-angle-right"></i>
                                                                                    <span>Request</span>
                                                                              </a>
                                                                        </li>
                                                                        <li class="{{ $matcher_list_active }}">
                                                                              <a href="/agent/matcher/list" class="{{ $matcher_list_active }}">
                                                                                    <i class="fa fa-angle-right"></i>
                                                                                    <span>List</span>
                                                                              </a>
                                                                        </li>
                                                                        <li class="">
                                                                              <a href="#" class="">
                                                                                    <b class="badge bg-danger pull-right">99+</b>
                                                                                    <i class="fa fa-angle-right"></i>
                                                                                    <span>Result</span>
                                                                              </a>
                                                                        </li>
                                                                  </ul>
                                                            </li>
                                                      </ul>
                                                </nav>
                                                <!-- / nav -->
                                          </div>
                                    </section>
                                    <footer class="footer lt hidden-xs b-t b-dark">
                                          <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon">
                                                <i class="fa fa-angle-left text"></i>
                                                <i class="fa fa-angle-right text-active"></i>
                                          </a>
                                    </footer>
                              </section>
                              </aside>
                              <!-- /.aside -->
                              @yield('content')
                        </section>
                  </section>
            </section>
            
            <div id="ajaxBModalOne" class="modal fade">
                  <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                              <div id="ajaxBModalOneBody" class="modal-body"></div>
                        </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
            </div>
                  
            <!-- <script src="{{ URL::asset('js/jquery.min.js') }}"></script>-->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
            <!-- Bootstrap -->
            <script src="{{ URL::asset('js/bootstrap.js') }}"></script>
            <!-- App -->
            <script src="{{ URL::asset('js/app.js') }}"></script>
            <script src="{{ URL::asset('js/app.plugin.js') }}"></script>
            <script src="{{ URL::asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>
            <!-- fuelux -->
            <script src="{{ URL::asset('js/fuelux/fuelux.js') }}"></script>
            <!-- Custom -->
            <script src="{{ URL::asset('js/custom/master.js') }}"></script>
            <script src="{{ URL::asset('js/custom/matcher.js') }}"></script>
      </body>
</html>