
<!-- load master layout -->
@extends('agent.master')

<!-- define title -->
@section('title', 'Dashboard')      

<!-- load content -->
@section('content')
      <section id="content">
            <section class="vbox">
                  <section class="scrollable padder">
                        <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                              <li><a href="/agent/dashboard"><i class="fa fa-home"></i>Home</a></li>
                              <li class="active">Dashboard</li>
                        </ul>
                        <div class="m-b-md">
                              <h3 class="m-b-none">Dashboard</h3>
                              <small>Welcome back, {{ $display_name }}</small>
                        </div>
                        <section class="panel panel-default">
                              <div class="row m-l-none m-r-none bg-light lter">
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light lt">
                                          <span class="fa-stack fa-2x pull-left m-r-sm">
                                                <i class="fa fa-circle fa-stack-2x text-warning"></i>
                                                <i class="fa fa-male fa-stack-1x text-white"></i>
                                          </span>
                                          <a class="clear" href="#">
                                                <span class="h3 block m-t-xs"><strong>{{ $active_case }}</strong></span>
                                                <small class="text-muted text-uc">client follow-up</small>
                                          </a>
                                    </div>
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                                          <span class="fa-stack fa-2x pull-left m-r-sm">
                                                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                <i class="fa fa-check fa-stack-1x text-white"></i>
                                          </span>
                                          <a class="clear" href="#">
                                                <span class="h3 block m-t-xs"><strong>{{ $successful_followup }}</strong></span>
                                                <small class="text-muted text-uc">successful followup</small>
                                          </a>
                                    </div>
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light lt">
                                          <span class="fa-stack fa-2x pull-left m-r-sm">
                                                <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                                <i class="fa fa-question fa-stack-1x text-white"></i>
                                          </span>
                                          <a class="clear" href="#">
                                                <span class="h3 block m-t-xs"><strong>{{ $active_case }}</strong></span>
                                                <small class="text-muted text-uc">potential client</small>
                                          </a>
                                    </div>
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                                          <span class="fa-stack fa-2x pull-left m-r-sm">
                                                <i class="fa fa-circle fa-stack-2x text-dark"></i>
                                                <i class="fa fa-lock fa-stack-1x text-white"></i>
                                          </span>
                                          <a class="clear" href="#">
                                                <span class="h3 block m-t-xs"><strong>{{ $active_case }}</strong></span>
                                                <small class="text-muted text-uc">kpi to hit</small>
                                          </a>
                                    </div>
                              </div>
                        </section>
                  </section>
              </section>
      </section>
@endsection