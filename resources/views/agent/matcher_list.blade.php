
<!-- load master layout -->
@extends('agent.master')

<!-- define title -->
@section('title', 'Pattern Matcher')
      
<!-- load content -->
@section('content')
      <section id="content">
            <section class="vbox">
                  <section class="scrollable padder">
                        <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                              <li><a href="/agent/dashboard"><i class="fa fa-home"></i>Home</a></li>
                              <li><a href="#">Pattern Matcher</a></li>
                              <li class="active"><a href="#">List</a></li>
                        </ul>
                        <div class="m-b-md">
                              <h3 class="m-b-none">Pattern Request List</h3>
                              <small>Welcome back, {{ $display_name }}</small>
                        </div>
                        <div id="property_matcher_list">
                              <section class="panel panel-default">
                                    <table class="table table-striped m-b-none">
                                          <thead>
                                                <tr>
                                                      <th class="text-center">No</th>
                                                      <th class="text-center">Name</th>
                                                      <th class="text-center">Contact</th>
                                                      <th class="text-center">Type</th>
                                                      <th class="text-center">District</th>
                                                      <th class="text-center">Room(s)</th>
                                                      <th class="text-center">Others</th>
                                                      <th class="text-center"></th>
                                                </tr>
                                          </thead>
                                          <tbody>
                                                @foreach ($pattern_lists as $pattern_list)
                                                      <tr>
                                                            <td class="text-center">{{ $pattern_list->no }}</td>
                                                            <td>{{ $pattern_list->customer_name }}</td>
                                                            <td>{{ $pattern_list->customer_contact }}</td>
                                                            <td class="text-center">{{ $pattern_list->property_type }}</td>
                                                            <td>{{ $pattern_list->district }}</td>
                                                            <td class="text-center">{{ $pattern_list->room }}</td>
                                                            <td>{{ $pattern_list->others }}</td>
                                                            <td class="text-center">
                                                                  <button class="btn btn-primary btn-xs" onclick="viewMatchDetails('{{ $pattern_list->id }}');">View</button>
                                                                  <button class="btn btn-warning btn-xs" onclick="editMatchDetails('{{ $pattern_list->id }}');">Edit</button>
                                                                  <button class="btn btn-danger btn-xs" onclick="removeMatchDetails('{{ $pattern_list->id }}');">Remove</button>
                                                            </td>
                                                      </tr>
                                                @endforeach
                                          </tbody>
                                    </table>
                              </section>
                        </div>
                  </section>
            </section>
      </section>
@endsection