
<!-- load master layout -->
@extends('agent.master')

<!-- define title -->
@section('title', 'Pattern Matcher')

<!-- load content -->
@section('content')
      <section id="content">
            <section class="vbox">
                  <section class="scrollable padder">
                        <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                              <li><a href="/agent/dashboard"><i class="fa fa-home"></i>Home</a></li>
                              <li><a href="#">Pattern Matcher</a></li>
                              <li class="active"><a href="#">Form</a></li>
                        </ul>
                        <div class="m-b-md">
                              <h3 class="m-b-none">Pattern Matcher Form</h3>
                              <small>Welcome back, {{ $display_name }}</small>
                        </div>
                        <div id="property_matcher_form">
                              <section class="panel panel-default">
                                    <header class="panel-heading font-bold">                  
                                    Pattern Matcher Form
                                    </header>
                                    <div class="panel-body">
                                          <form class="form-horizontal" role="form">
                                                <br />
                                                <!-- Customer Name -->
                                                <div class="form-group">
                                                      <label class="col-sm-2 control-label" for="customer_name">Customer Name</label>
                                                      <div class="col-sm-9">
                                                            <input type="text" id="customer_name" name="customer_name" class="form-control" placeholder="Customer Name">
                                                      </div>
                                                      <div class="col-md-1"></div>
                                                </div>
                                                <div class="line line-dashed line-lg pull-in"></div>
                                                <!-- Customer Contact -->
                                                <div class="form-group">
                                                      <label class="col-sm-2 control-label" for="customer_contact">Customer Contact</label>
                                                      <div class="col-sm-9">
                                                            <input type="text" id="customer_contact" name="customer_contact" class="form-control" placeholder="Customer Contact">
                                                      </div>
                                                      <div class="col-md-1"></div>
                                                </div>
                                                <div class="line line-dashed line-lg pull-in"></div>
                                                <!-- Property Type -->
                                                <div class="form-group">
                                                      <label class="col-sm-2 control-label" for="property_type">Property Type</label>
                                                      <div class="col-sm-9">
                                                            <select id="property_type" name="property_type" class="form-control m-b">
                                                                  <option value="0">-- Select One --</option>
                                                                  <option value="1">Apartment</option>
                                                                  <option value="2">Flat</option>
                                                                  <option value="3">Condominium</option>
                                                                  <option value="4">Service Residence</option>
                                                                  <option value="5">Bungalow</option>
                                                                  <option value="6">Semi-detached House</option>
                                                                  <option value="7">Cluster House</option>
                                                                  <option value="8">Bungalow Land</option>
                                                                  <option value="5">Terrace</option>
                                                            </select>
                                                            <!--<input type="text" id="property_type" name="property_type" class="form-control" placeHolder="Property Type">-->
                                                      </div>
                                                      <div class="col-md-1"></div>
                                                </div>
                                                <div class="line line-dashed line-lg pull-in"></div>
                                                <!-- District -->
                                                <div class="form-group">
                                                      <label class="col-sm-2 control-label" for="district">District</label>
                                                      <div class="col-sm-9">
                                                            <select id="district" name="district" class="form-control m-b">
                                                                  <option value="0">-- Select One --</option>
                                                                  <option value="unknown">Unknown</option>
                                                                  <option value="unknown_ii">Unknown II</option>
                                                            </select>
                                                      </div>
                                                      <div class="col-md-1"></div>
                                                </div>
                                                <div class="line line-dashed line-lg pull-in"></div>
                                                <!-- Room(s) -->
                                                <div class="form-group">
                                                      <label class="col-sm-2 control-label" for="room">Room(s)</label>
                                                      <div class="col-sm-9">
                                                            <select id="room" name="room" class="form-control m-b">
                                                                  <option value="0">-- Select One --</option>
                                                                  <option value="1+">1+</option>
                                                                  <option value="2+">2+</option>
                                                                  <option value="3+">3+</option>
                                                                  <option value="4+">4+</option>
                                                                  <option value="5+">5+</option>
                                                            </select>
                                                      </div>
                                                      <div class="col-md-1"></div>
                                                </div>
                                                <div class="line line-dashed line-lg pull-in"></div>
                                                <!-- other Details -->
                                                <div class="form-group">
                                                      <label class="col-sm-2 control-label" for="criteria">5. Other Details</label>
                                                      <div class="col-sm-9">
                                                            <input type="text" id="other_details" name="other_details" class="form-control" placeholder="Other Details">
                                                      </div>
                                                      <div class="col-md-1"></div>
                                                </div>
                                                <div class="line line-dashed line-lg pull-in"></div>
                                                <div class="form-group">
                                                      <div class="col-md-10"></div>
                                                      <div class="col-md-2">
                                                            <button type="button" class="btn btn-s-md btn-default" onclick="insertCriteria();">Insert</button>
                                                      </div>
                                                </div>
                                          </form>
                                    </div>
                              </section>
                        </div>
                  </section>
            </section>
      </section>
@endsection