<!DOCTYPE html>
<html lang="en" class="">
      <head>
            <meta charset="utf-8" />
            <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
            
            <title>Panel | 404</title>
            
            <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ URL::asset('css/font.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" type="text/css" />
            
            <!--[if lt IE 9]>
			<script src="{{ URL::asset('js/ie/html5shiv.js') }}"></script>
			<script src="{{ URL::asset('js/ie/respond.min.js') }}"></script>
			<script src="{{ URL::asset('js/ie/excanvas.js') }}"></script>
		<![endif]-->
      </head>
      <body>
            <section id="content">
                  <div class="row m-n">
                        <div class="col-sm-4 col-sm-offset-4">
                              <div class="text-center m-b-lg">
                                    <h1 class="h text-white animated fadeInDownBig">404</h1>
                              </div>
                              <div class="list-group m-b-sm bg-white m-b-lg">
                                    <a href="index.html" class="list-group-item">
                                          <i class="fa fa-chevron-right icon-muted"></i>
                                          <i class="fa fa-fw fa-home icon-muted"></i> Goto homepage
                                    </a>
                                    <a href="#" class="list-group-item">
                                          <i class="fa fa-chevron-right icon-muted"></i>
                                          <i class="fa fa-fw fa-question icon-muted"></i> Send us a tip
                                    </a>
                                    <a href="#" class="list-group-item">
                                          <i class="fa fa-chevron-right icon-muted"></i>
                                          <span class="badge">021-215-584</span>
                                          <i class="fa fa-fw fa-phone icon-muted"></i> Call us
                                    </a>
                              </div>
                        </div>
                  </div>
            </section>
            <!-- footer -->
            <footer id="footer">
                  <div class="text-center padder clearfix">
                        <p>
                              <small>Mobile first web app framework base on Bootstrap<br>&copy; 2013</small>
                        </p>
                  </div>
            </footer>
            <!-- / footer -->
            
            <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
		<!-- Bootstrap -->
		<script src="{{ URL::asset('js/bootstrap.js') }}"></script>
		<!-- App -->
		<script src="{{ URL::asset('js/app.js') }}"></script>
		<script src="{{ URL::asset('js/app.plugin.js') }}"></script>
		<script src="{{ URL::asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>
                  
      </body>
</html>
      