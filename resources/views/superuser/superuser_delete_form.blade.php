<html>
	<form method="post" action="/superuser/auth/delete/superuser/{{$id}}">
		@csrf
		<h1>Delete Superuser</h1>
		<div class="row">
			<div class="col-md-12">
				<label>ID : </label> 
				<select name='id'>
				@foreach ($superusers as $superuser)
					<option value={{$superuser['id']}}>{{$superuser['id']}}</option>
				@endforeach
				</select>
			</div>
		</div>
		<input type="submit" value="Submit">
	</div>
</html>