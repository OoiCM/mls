@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul>
                        <li>Manage Superuser
                            <ul>
                                <li><a href="http://127.0.0.1:8000/superuser/auth/show/{{$id}}">View Superuser</li>
                                <li><a href="http://127.0.0.1:8000/superuser/auth/show_all">View All Superuser</li>
                                <li><a href="http://127.0.0.1:8000/superuser/auth/create/form/{{$id}}">Create Superuser</li>
                                <li><a href="http://127.0.0.1:8000/superuser/auth/edit/form/{{$id}}">Edit Superuser</li>
                                <li><a href="http://127.0.0.1:8000/superuser/auth/delete/form/{{$id}}">Delete Superuser</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
