<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agent\Agent;    // Modal

class LogoutController extends Controller{
    
      /*
      *     Public Process
      */
      
      /*
      *     Logout process
      *     - function that run when logout
      */
      public function agentLogoutProcess(){
            Agent::where('id', session('agent_id'))->update(['is_login' => 0]);
            session()->flush();     // clear and remove all session
            return redirect('agent/login');
      }
}
