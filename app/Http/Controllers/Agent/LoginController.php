<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Agent\Agent;    // Modal


class LoginController extends Controller{
      
      /*
      *     Public Process
      */
      
      /*
      *     Index function
      *     - default function when the page load
      */
      public function index(){
            return view('agent.login', []);
      }
      
      /*
      *     Login process
      *     - function that run when login
      */
      public function agentLoginProcess(Request $request){
            $redirect = redirect('agent/login');  // default redirect
            $id = $request->input('id');
            
            if(strlen($id) == 5){
                  $id = ltrim($id, 'A');
                  if(is_numeric(ltrim($id, '0'))){
                        $encryptedPass = Hash::make($request->input('password'));
                        $agent = Agent::getAgentLoginDetailProcess($id);
                        if (Hash::check($request->input('password'), $agent->password)){
                              // Check whether agent has login from other laptop or forget to logout
                              if($agent->is_login != 1){
                                    // No longer login, able to proceed
                                    Agent::updateAgentLoginDetailProcess($id); // Update agent login data
                                    self::assignSessionProcess($agent); // Assign session
                                    return redirect('agent/dashboard');
                              }
                        }
                  }
            }
            
            return $redirect;
      }
      
      /*
      *     Assign session
      *     - assign session when successfully login
      */
      public function assignSessionProcess($agent){
            session(['agent_id' => $agent->id]);      // need hash agent_id
            session(['display_name' => $agent->display_name]);
      }

}