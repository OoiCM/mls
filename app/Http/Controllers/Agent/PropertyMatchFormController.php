<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Agent\TrackController; // Controller
use App\Agent\Role;     // Modal
use App\Agent\PropertyCriteria;     // Modal

class PropertyMatchFormController extends Controller {
      
      /*
      *     Public Process
      */
      
      /*
      *     Index function
      *     - default function when the page load
      */
      public function index(){
            if(session()->has('agent_id')){
                  if(!empty(Role::checkModuleAccessibility(request()->path()))){
                        $insert = TrackController::insertActivityTrack(session('agent_id'), 'Page Loaded');
                        return view('agent.matcher_form', ['display_name' => session('display_name'),
                                                           'dashboard_active' => '',
                                                           'matcher_active' => 'active',
                                                           'matcher_form_active' => 'active',
                                                           'matcher_list_active' => '']);
                  }
                  else{
                        // Dunno where to route yet
                  }
            }
            else{
                  return redirect('agent/login');
            }
      }
      
      /*
      *     Insert Match Function
      *     - function that insert match into database
      */
      public function insertPatternProcess(Request $request){
            if(session()->has('agent_id')){
                  if(PropertyCriteria::getActivePatternCount(session('agent_id')) >= 3){
                        return response()->json(['status'=>'200',
                                                 'message'=>'Search pattern exceed']);
                  }
                  else{
                        $criteria = (object)array();
                        $criteria->customer_name = $request->customer_name;
                        $criteria->customer_contact = $request->customer_contact;
                        $criteria->property_type = $request->property_type;
                        $criteria->district = $request->district;
                        $criteria->criteria = (object)array();
                        $criteria->criteria->room = $request->room;
                        $criteria->criteria->others = $request->other_details;
                        
                        $propertyMatch = new PropertyCriteria();
                        $propertyMatch->agent_id = session('agent_id');
                        $propertyMatch->property_type = $request->property_type;
                        $propertyMatch->criteria = json_encode($criteria);
                        $propertyMatch->status = 1;
                        $stat = $propertyMatch->save();
                        
                        return response()->json(['status'=>'200',
                                                 'message'=>'Criteria added']);
                  }
                  
                  /*
                  {
                        "name" : "abc",
                        "type": "",
                        "district": "",
                        "criteria": { "room": 3, "adv_search" : "Living" }
                  }
                        maximum 3 search
                        new table to store matched property, unmatched property
                  */
            }
            else{
                  return response()->json(['status'=>'200',
                                           'message'=>'You do not have permission to access this action']);
            }
      }
      
}
