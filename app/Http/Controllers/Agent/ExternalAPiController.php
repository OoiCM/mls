<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Agent\TrackController; // Controller
use App\Agent\Agent;    // Modal
use App\Agent\ActivityLog;    // Modal
use Carbon\Carbon;      // Date time manipulation class


class ExternalAPiController extends Controller {
      
      /*
      *     Public Process
      */
      
      /*
      *     Index function
      *     - default function when the page load
      */
      public function index(){
            // Do Nothing
      }
      
      /*
      *     Insert agent
      *     - used to insert agent
      */
      public function createAgentProcess(Request $request){
            $agent = new Agent();
            $agent->username = $request->username;
            $agent->email = $request->email;
            $agent->password = Hash::make($request->password);
            $agent->display_name = $request->display_name;
            $agent->created_by = $request->created_by;      // Run a function to use api to get manager id
            
            $stat = $agent->save();
            $message = self::apiReturnMessage(200);
            
            return response()->json(['code'=>200,
                                     'message'=>$message,
                                     'data'=>true]);
      }
      
      /*
      *     Get agent
      *     - get single agent based on id
      */
      public function getAgentProcess(Request $request){
            $id = $request->id;
            $agent = Agent::getAgentLoginDetailProcess($id);
            if(!empty($agent)){
                  $obj = (object)array();
                  $obj->display_name = $agent->display_name;
                  $message = self::apiReturnMessage(200);
                  return response()->json(['code'=>200,
                                           'message'=>$message,
                                           'data'=>$obj]);
            }
            else{
                  $message = self::apiReturnMessage(200);
                  return response()->json(['code'=>200,
                                           'message'=>$message,
                                           'data'=>'No record found']);
            }
      }
      
      /*
      *     Edit agent
      *     - edit single agent based on id
      */
      public function editAgentProcess(Request $request){
            $id = $request->id;
            $code = 200;
            $agent = new Agent();
            
            $agent = $agent->find($id);
            if(!is_null($agent)){
                  // update email
                  if(!empty($request->email)) $agent->email = $request->email;
                  
                  // update username
                  if(!empty($request->username)) $agent->username = $request->username;
                  
                  // update display name
                  if(!empty($request->display_name)) $agent->display_name = $request->display_name;
                  
                  $agent->getDirty();     // Get old and new value
                  TrackController::insertActivityTrack($id, json_encode($agent->getDirty()));
                  $agent->save();
                  $message = self::apiReturnMessage($code);
                  return response()->json(['code'=>$code,
                                           'message'=>$message,
                                           'data'=>'Agent detail updated']);
            }
            else{
                  $message = self::apiReturnMessage($code);
                  return response()->json(['code'=>$code,
                                           'message'=>$message,
                                           'data'=>'Agent not found']);
            }
            
            $message = self::apiReturnMessage($code);
            return response()->json(['code'=>$code,
                                     'message'=>$message,
                                     'data'=>false]);
      }
      
      /*
      *     Delete agent
      *     - delete single agent based on id
      */
      public function deleteAgentProcess(Request $request){
            $id = $request->id;
            $code = 200;
            $agent = new Agent();
            
            $agent = $agent->find($id);
            if(!is_null($agent)){
                  $agent->status = 0;
                  $agent->getDirty();     // Get old and new value
                  TrackController::insertActivityTrack($id, json_encode($agent->getDirty()));
                  $agent->save();
                  $message = self::apiReturnMessage($code);
                  return response()->json(['code'=>$code,
                                           'message'=>$message,
                                           'data'=>'Agent deleted']);
            }
            
            $message = self::apiReturnMessage($code);
            return response()->json(['code'=>$code,
                                     'message'=>$message,
                                     'data'=>'No agent found']);
      }
      
      /*
      *     To create schedule
      *     - to create schedule
      */
      public function createScheduleProcess(Request $request){
            $activityLog = new ActivityLog();
            $activityLog->agent_id = $request->id;
            $activityLog->activity_remarks = $request->activity_remarks;
            $activityLog->activity_date = Carbon::now()->toDateTimeString();
            $activityLog->type = 11;
            // $activityLog->change_log = json_encode($activityLog->getDirty());
            
            $stat = $activityLog->save();
            $message = self::apiReturnMessage(200);
            
            return response()->json(['code'=>200,
                                     'message'=>$message,
                                     'data'=>true]);
      }
      
      /*
      *     To get schedule
      *     - to get all schedule based on that agent id
      */
      public function getScheduleProcess(Request $request){
            if(!empty($request->agent_id)){
                  $limit = 10;
                  $offset = 0;
                  if(!empty($request->limit)){
                        $limit = $request->limit;
                  }
                  if(!empty($request->offset)){
                        $offset = $request->offset;
                  }
                  $activityLogs = ActivityLog::getActivityLogProcess($request->agent_id, $limit, $offset);
                  if(!empty($activityLogs)){
                        $logs = array();
                        foreach($activityLogs as $activityLog){
                              $obj = (object)array();
                              $obj->activity_remarks = $activityLog->activity_remarks;
                              $obj->activity_date = $activityLog->activity_date;
                              
                              array_push($logs, $obj);
                        }
                        
                        $message = self::apiReturnMessage(200);
                        return response()->json(['code'=>200,
                                                 'message'=>$message,
                                                 'data'=>$logs]);
                  }
                  else{
                        $message = self::apiReturnMessage(200);
                        return response()->json(['code'=>200,
                                                 'message'=>$message,
                                                 'data'=>'No record found']);
                  }
            }
      }
      
      /*
      *     To edit schedule
      *     - to edit schedule based on that track id
      */
      public function editScheduleProcess(Request $request){
            $id = $request->id;
            $code = 200;
            $activityLog = new ActivityLog();
            
            if(!empty($request->id)){
                  if(!empty($request->activity_remarks)){
                        $activityLog = $activityLog->find($id);
                        if(!is_null($activityLog)){
                              $activityLog->activity_remarks = $request->activity_remarks;
                              $activityLog->change_log = json_encode($activityLog->getDirty());     // Get old and new value
                              TrackController::insertActivityTrack($id, json_encode($activityLog->getDirty()));
                              $activityLog->save();
                              $message = self::apiReturnMessage($code);
                              return response()->json(['code'=>$code,
                                                       'message'=>$message,
                                                       'data'=>true]);
                        }
                        else{
                              $message = self::apiReturnMessage($code);
                              return response()->json(['code'=>$code,
                                                       'message'=>$message,
                                                       'data'=>'No schedule found']);
                        }
                  }
                  else{
                        $message = self::apiReturnMessage($code);
                        return response()->json(['code'=>$code,
                                                 'message'=>$message,
                                                 'data'=>'Please enter remark']);
                  }
            }
            else{
                  $message = self::apiReturnMessage($code);
                  return response()->json(['code'=>$code,
                                           'message'=>$message,
                                           'data'=>'No schedule found']);
            }
      }
      
      /*
      *     To delete schedule
      *     - to delete schedule based on that track id
      */
      public function deleteScheduleProcess(Request $request){
            $id = $request->id;
            $code = 200;
            $deleted_by = $request->deleted_by; // can be either agent itself or manager
            $status = ActivityLog::deleteScheduleProcess($id, $deleted_by);
            $message = self::apiReturnMessage($code);
            return response()->json(['code'=>$code,
                                     'message'=>$message,
                                     'data'=>$status]);
      }
      
      /*
      *     Private Process
      */
      
      /*
      *     APi return message code
      *     - used to generate message based on code
      */
      private function apiReturnMessage($code){
            $message = "";
            switch($code) {
                  case '200':
                        $message = "Command completed successfully.";
                  break;
                  case '400':
                        $message = "Bad Request.";
                  break;
                  case '403':
                        $message = "Unauthorized.";
                  break;
                  case '404':
                        $message = "File Not Found.";
                  break;
                  case '500':
                        $message = "Internal Server Error.";
                  break;
            } 
            return $message;
      }
      
}
