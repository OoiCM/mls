<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Agent\TrackController; // Controller
use App\Agent\ClientHandling;    // Modal
use App\Agent\Role;     // Modal

class DashboardController extends Controller {
    
      /*
      *     Public Process
      */
      
      /*
      *     Index function
      *     - default function when the page load
      */
      public function index(){
            if(session()->has('agent_id')){
                  if(!empty(Role::checkModuleAccessibility(request()->path()))){
                        $active_case = ClientHandling::getActiveCase(session('agent_id'));
                        $successful_followup = ClientHandling::getSuccessfulCase(session('agent_id'));
                        $insert = TrackController::insertActivityTrack(session('agent_id'), 'Page Loaded');
                        return view('agent.dashboard', ['display_name' => session('display_name'),
                                                        'dashboard_active' => 'active',
                                                        'matcher_active' => '',
                                                        'matcher_form_active' => '',
                                                        'matcher_list_active' => '',
                                                        'active_case' => $active_case,
                                                        'successful_followup' => $successful_followup]);
                  }
                  else{
                        // Dunno where to route yet
                  }
            }
            else{
                  return redirect('agent/login');
            }
      }
      
}
