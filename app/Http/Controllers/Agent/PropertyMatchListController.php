<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Route
use App\Http\Controllers\Controller;
use App\Http\Controllers\Agent\TrackController; // Controller
use App\Agent\Role;     // Modal
use App\Agent\PropertyCriteria;     // Modal

class PropertyMatchListController extends Controller {
      
      /*
      *     Public Process
      */
      
      /*
      *     Index function
      *     - default function when the page load
      */
      public function index(){
            if(session()->has('agent_id')){
                  if(!empty(Role::checkModuleAccessibility(request()->path()))){
                        $insert = TrackController::insertActivityTrack(session('agent_id'), 'Page Loaded');
                        $active_matcher_list = self::getActivePatternList();
                        return view('agent.matcher_list', ['display_name' => session('display_name'),
                                                           'dashboard_active' => '',
                                                           'matcher_active' => 'active',
                                                           'matcher_form_active' => '',
                                                           'matcher_list_active' => 'active',
                                                           'pattern_lists' => $active_matcher_list]);
                  }
                  else{
                        // Dunno where to route yet
                  }
            }
            else{
                  return redirect('agent/login');
            }
      }
      
      /*
      *     Get pattern list
      *     - get current agent pattern list
      */
      public function getActivePatternList(){
            $match_data = array();
            $patterns = PropertyCriteria::getActivePatternData(session('agent_id'));
            $no = 1;
            
            foreach($patterns as $pattern){
                  $pattern_id = $pattern->id;
                  if(!empty($pattern->criteria)){
                        // Format data
                        $data = json_decode($pattern->criteria);
                        $obj = (object)array();
                        $obj->no = $no;
                        $obj->id = $pattern_id;
                        $obj->customer_name = $data->customer_name;
                        $obj->customer_contact = $data->customer_contact;
                        $obj->property_type = $data->property_type;
                        $obj->district = $data->district;
                        $obj->room = $data->criteria->room;
                        $obj->others = $data->criteria->others;
                        $no++;
                        
                        array_push($match_data, $obj);
                  }
            }
            
            return $match_data;
      }
      
      /*
      *     Remove pattern list
      *     - remove a pattern list to free up a slot
      */
      public function removePatternProcess(Request $request){
            if(session()->has('agent_id')){
                  PropertyCriteria::removePatternData($request->id);    ///////////////////
            }
      }
      
      
}
