<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agent\ActivityLog;     // Modal

class LogController extends TrackController {
      
      /*
      *     Public Process
      */
      
      /*
      *     Index function
      *     - default function when the page load
      */
      public function index(){
            
      }
      
      /*
      *     Insert Remark Function
      *     - function that insert remark into database
      */
      public function insertLogProcess(Request $request){
            if(session()->has('agent_id')){
                  $activityLog = new ActivityLog();
                  $activityLog->agent_id = session('agent_id');
                  $activityLog->activity_remarks = $request->log;
                  $activityLog->type = 11;
                  $activityLog->change_log = '';
                  $stat = $activityLog->save();
                  
                  return response()->json(['status'=>'200',
                                           'message'=>'Log inserted']);
            }
            else{
                  return response()->json(['status'=>'200',
                                           'message'=>'You do not have permission to access this action']);
            }
      }
      
}
