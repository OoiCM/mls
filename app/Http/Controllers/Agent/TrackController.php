<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agent\ActivityTrack;     // Modal

class TrackController extends Controller {
      
      /*
      *     Public Process
      */
      
      /*
      *     Insert track log
      *     - insert activity log when redirect to any page
      */
      public static function insertActivityTrack($agent_id, $action_detail = '', $action_value = ''){
            // Only insert log when in live
            if(getenv('RUNON') == 'LIVE'){      // isDirty / getDirty
                  $agentTrack = new ActivityTrack();
                  $agentTrack->agent_id = $agent_id;
                  $agentTrack->action_from = request()->path();   // get current url without domain
                  $agentTrack->action_details = $action_detail;   // set a standardize event
                  $agentTrack->action_value = $action_value;      // value that inserted when insert, update or delete
                  
                  /*
                  1. Page Loaded - empty value
                  2. Create / Insert new record - { [:table_name:] : [:id_created:] }
                  3. Read / Select Retrieve row of/single record(s) - { [:table_name:] }
                  4. Update existing record - { [:table_name:] : [:id_updated:] : [:old_value:] -> [:new_value:] }
                  5. Delete existing record - { [:table_name:] : [:id_deleted:] }
                  */
                  
                  $stat = $agentTrack->save();
            }
      }
      
}
