<?php

namespace App\Root;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RootLog extends Model
{
    use SoftDeletes;

    protected $table = 'mls_root_log';
    protected $guarded = [];

    // public function adminDetail(){
    // 	return $this->hasOne('App\AdminDetail','admin_id');
    // }
}
