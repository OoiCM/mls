<?php

namespace App\Root;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Root extends Model
{
    use SoftDeletes;

    protected $table = 'mls_root';
    protected $guarded = [];

    // public function superuserDetail(){
    // 	return $this->hasOne('App\SuperuserDetail','superuser_id');
    // }
}
