<?php

namespace App\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;      // Date time manipulation class

class ActivityLog extends Model {
      
      /*
      *     Constant variable
      */
      protected $table = 'mls_agent_activity_log';
      protected $fillable = ['agent_id', 'activity_remarks', 'type', 'is_deleted', 'change_log'];
      public $timestamps = false;
      // const CREATED_AT = 'activity_date';
      // public $updated_at = '';
      
      /*
      *     Public Process
      */
      
      /*
      *     Get activity log
      *     - select agent's detail based on id
      */
      public static function getActivityLogProcess($agent_id, $limit, $offset){
            $activityLog = self::select('activity_remarks', 'activity_date')->where('agent_id', '=', $agent_id)->limit($limit)->offset($offset)->get();
            return $activityLog;
      }
      
      /*
      *     Delete single activity log
      *     - delete log
      */
      public static function deleteScheduleProcess($id, $deleted_by){
            $status = self::where('id', '=', $id)->update(['is_deleted' => 1, 'deleted_at' => Carbon::now()->toDateTimeString(), 'deleted_by' => $deleted_by]);
            return $status;
      }
      
      
      // Set activity type (remarks or schedule)
}
