<?php

namespace App\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityTrack extends Model {
      
      /*
      *     Constant variable
      */
      protected $table = 'mls_agent_activity_track';
      protected $fillable = ['agent_id', 'action_from', 'action_details', 'action_date'];
      // public $timestamps = false;
      const CREATED_AT = 'action_date';
      public $updated_at = '';
      
      /*
      *     Public Process
      */
      
}
