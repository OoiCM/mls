<?php

namespace App\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientHandling extends Model {
      
      /*
      *     Constant variable
      */
      protected $table = 'mls_agent_client_handling';
      protected $fillable = ['client_id', 'agent_id', 'date_started', 'status'];
      public $timestamps = false;
      
      /*
      *     Public Process
      */
      
      /*
      *     Check active case
      *     - get how many clients is this agent curently handling
      */
      public static function getActiveCase($agent_id){
            $active_case = 0;
            if(!empty($agent_id) && $agent_id != 0){
                  $active_case = self::where('agent_id', '=', $agent_id)->where('status', '=', 1)->count();
            }
            return $active_case;
      }
      
       /*
      *     Check successful case
      *     - get how many clients is this agent curently handling
      */
      public static function getSuccessfulCase($agent_id){
            $successful_case = 0;
            if(!empty($agent_id) && $agent_id != 0){
                  $successful_case = self::where('agent_id', '=', $agent_id)->where('status', '=', 1)->whereNull('date_ended')->count();
            }
            return $successful_case;
      }
}
