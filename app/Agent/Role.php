<?php

namespace App\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model {
      
      /*
      *     Constant variable
      */
      protected $table = 'mls_agent_role';
      protected $fillable = ['module_path', 'module_accessible', 'module_status', 'created_by'];
      // public $timestamps = false;
      const CREATED_AT = 'created_date';
      public $updated_at = '';
      
      /*
      *     Public Process
      */
      
      /*
      *     Check accessibily
      *     - To check whether agent is able to access that particular module or no     
      */
      public static function checkModuleAccessibility($path){
            $module = true;
            // Only check module when in live
            if(getenv('RUNON') == 'LIVE'){
                  $module = self::select('id')->where('module_path', '=', $path)->where('module_status', '=', 1)->where('module_accessible', '=', 1)->first();
            }
            return $module;
      }
}
