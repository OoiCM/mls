<?php

namespace App\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;      // Date time manipulation class

class PropertyCriteria extends Model {
      
      /*
      *     Constant variable
      */
      protected $table = 'mls_property_criteria_pattern';
      protected $fillable = ['agent_id', 'property_type', 'criteria', 'status'];
      public $timestamps = false;
      // const CREATED_AT = 'date_added';
      // public $updated_at = '';
      
      /*
      *     Public Process
      */
      
      /*
      *     Check active pattern
      *     - get how many pattern an agent currently having
      */
      public static function getActivePatternCount($agent_id){
            $count = 0;
            if(!empty($agent_id) && $agent_id != 0){
                  $count = self::where('agent_id', '=', $agent_id)->where('status', '=', 1)->count();
            }
            return $count;
      }
      
      /*
      *     Get active pattern list
      *     - get pattern list belong to current login agent
      */
      public static function getActivePatternData($agent_id){
            $active_matcher_list = array();
            if(!empty($agent_id) && $agent_id != 0){
                  $active_matcher_list = self::select('id', 'criteria')->where('agent_id', '=', $agent_id)->where('status', '=', 1)->get();
            }
            return $active_matcher_list;
      }
      
      /*
      *     Remove active pattern
      *     - remove any active pattern 
      */
      public static function removePatternData($id){  /////////////////
            /*
            // Update
            $sql = "UPDATE mls_agent SET username = 'not test' WHERE agent_id = 1";
            Agent::where('agent_id', 1)->update(['username' => 'not test']);
            */
            $agent_id = session('agent_id');
            $status = false;
            if(!empty($agent_id) && $agent_id != 0){
                  $status = self::where('id', '=', $id)->update(['status' => '0', 'date_removed' => Carbon::now()->toDateTimeString()]);
            }
            return $status;
      }
      
}
