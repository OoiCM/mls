<?php

namespace App\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;      // Date time manipulation class

class Agent extends Model {
    
      /*
      *     Constant variable
      */
      protected $table = 'mls_agent';
      protected $fillable = ['username', 'email', 'password', 'display_name', 'last_login', 'status', 'temp_pass_status', 'is_login'];
      public $timestamps = false;
      
      /*
      *     Public Process
      */
      
      /*
      *     Get agent
      *     - select agent's detail based on id
      */
      public static function getAgentLoginDetailProcess($id){
            $agent = self::select('id', 'display_name', 'is_login', 'password')->where('id', '=', $id)->first();
            return $agent;
      }
      
      /*
      *     Update agent details
      *     - update is_login to true, update last_login to now (when login)
      */
      public static function updateAgentLoginDetailProcess($id){
            $status = self::where('id', '=', $id)->update(['is_login' => 1, 'last_login' => Carbon::now()->toDateTimeString()]);
            return $status;
      }
      
}