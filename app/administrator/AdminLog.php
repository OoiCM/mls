<?php

namespace App\Administrator;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminLog extends Model
{
    use SoftDeletes;

    protected $table = 'mls_admin_log';
    protected $guarded = [];

    // public function adminDetail(){
    // 	return $this->hasOne('App\AdminDetail','admin_id');
    // }
}
