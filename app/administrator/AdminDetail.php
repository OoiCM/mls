<?php

namespace App\Administrator;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class AdminDetail extends Model
{
	// use SoftDeletes;

    protected $table = 'mls_admin_detail';
    protected $guarded = [];
    const UPDATED_AT = null;
    
    public function admin()
    {
        return $this->belongsTo('App\administrator\Admin','admin_id');
    }
}
