<?php

namespace App\Administrator;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes;

    protected $table = 'mls_property_sell';
    protected $guarded = [];
}
