<?php

namespace App\Administrator;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Model
{
    use SoftDeletes;

    protected $table = 'mls_admin';
    protected $guarded = [];

    public function adminDetail(){
    	return $this->hasOne('App\administrator\AdminDetail','admin_id');
    }
}

// namespace App;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Database\Eloquent\SoftDeletes;

// class Admin extends Authenticatable
// {
//     use Notifiable;
//     use SoftDeletes;

//     protected $table = 'mls_admin';
//     /**
//      * The attributes that are mass assignable.
//      *
//      * @var array
//      */
//     protected $fillable = [
//         'username', 'password', 'status', 'permission', 'is_delete', 'deleted_at', 'created_by', 'created_at', 'updated_by', 'updated_at', 'last_login', 'remember_token'
//     ];

//     /**
//      * The attributes that should be hidden for arrays.
//      *
//      * @var array
//      */
//     protected $hidden = [
//         'password', 'remember_token',
//     ];

//     protected $guarded = [];

//     public function adminDetail(){
//     	return $this->hasOne('App\AdminDetail','admin_id');
//     }
// }
