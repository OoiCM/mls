<?php

namespace App\Administrator;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class AgentRequest extends Model
{
    //
    // use SoftDeletes;

    protected $table = 'mls_pending_agent_request';
    protected $fillable = ['client_id','status','is_approve','is_reject','created_at'];

    public static function laratablesOrderName()
    {
        return 'client_id';
    }

    // public function laratablesRowData()
    // {
    //     return [
    //         'id' => $this->id,
    //     ];
    // }
}
