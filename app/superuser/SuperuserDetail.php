<?php

namespace App\Superuser;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class SuperuserDetail extends Model
{
	// use SoftDeletes;

    protected $table = 'mls_superuser_detail';
    protected $guarded = [];
    const UPDATED_AT = null;
    
    public function superuser()
    {
        return $this->belongsTo('App\superuser\Superuser','superuser_id');
    }
}
