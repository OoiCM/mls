<?php

namespace App\Superuser;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuperuserLog extends Model
{
    use SoftDeletes;

    protected $table = 'mls_superuser_log';
    protected $guarded = [];

    // public function adminDetail(){
    // 	return $this->hasOne('App\AdminDetail','admin_id');
    // }
}
