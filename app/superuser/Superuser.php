<?php

namespace App\Superuser;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Superuser extends Model
{
    use SoftDeletes;

    protected $table = 'mls_superuser';
    protected $guarded = [];

    public function superuserDetail(){
    	return $this->hasOne('App\superuser\SuperuserDetail','superuser_id');
    }
}
