      
      // 0. Page onload function
	$(document).ready(function () {
            $.ajaxSetup({
                  headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
            });
	});
      
      // 1. Insert Criteria
      function insertCriteria(){
            var customer_name = $("#customer_name").val();
            var customer_contact = $("#customer_contact").val();
            var property_type = $("#property_type").val();
            var district = $("#district").val();
            var room = $("#room").val();
            var other_details = $("#other_details").val();
            
            var data = {
			customer_name : customer_name,
                  customer_contact : customer_contact,
			property_type : property_type,
                  district : district,
                  room : room,
                  other_details : other_details
		};
		$.ajax({
			type: "POST",
			url: "/agent/matcher/form",
			data: data,
			success: function(result){
                        alert(result.message);
                        $("#customer_name").val('');
                        $("#customer_contact").val('');
                        $("#property_type").val(0);
                        $("#district").val(0);
                        $("#room").val(0);
                        $("#other_details").val('');
			}
		});
      }
      
      // 2. Open Modal
      function viewMatchDetails(id){
            $("#ajaxBModalOne").modal('show');
      }
      
      // 3. Remove match details
      function removeMatchDetails(id){
            var data = {
                  id :id
            }
            $.ajax({
                  type: "POST",
                  url: "/agent/matcher/delete",
                  data: data,
                  success: function(result){
                        
                  }
            });
      }
      
      